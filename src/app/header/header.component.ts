import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  searchName = "";
  selectedOption: string;
  sorts = [
    { name: "Name(A-Z)", value: 1 },
    { name: "Name(Z-A)", value: 2 },
    { name: "Rank↑", value: 3 },
    { name: "Rank↓", value: 4 }
  ];
  @Output() messageEvent = new EventEmitter<string>();
  @Output() sortBy = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  sendMessage() {
    this.messageEvent.emit(this.searchName);
  }
  selectedOrder() {
    // alert("test");
    // console.log(this.selectedOption);
    this.sortBy.emit(this.selectedOption);
  }
}
