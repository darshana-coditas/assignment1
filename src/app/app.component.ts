import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "gitHub";

  public searchName: string;
  public sortByValue: string;
  public pNo: number;
  public totalCount: number;

  receiveName($event) {
    this.searchName = $event;
    // console.log(this.searchName);
  }
  receiveSortByValue($event) {
    this.sortByValue = $event;
    // console.log(this.sortByValue);
  }
  receivePageNum($event) {
    this.pNo = $event;
    // console.log("Helooo........Page number:" + this.pNo);
  }
  receiveTotalCount($event) {
    this.totalCount = $event;
    console.log("totalcount:" + this.totalCount);
  }
}
