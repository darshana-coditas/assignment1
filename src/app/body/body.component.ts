import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ListService } from "../list.service";

@Component({
  selector: "app-body",
  templateUrl: "./body.component.html",
  styleUrls: ["./body.component.css"]
})
export class BodyComponent implements OnInit {
  @Input() searchUserName: string;
  @Input() sortBy: string;
  @Input() pageNo: number;

  @Output() totalCount = new EventEmitter();

  // public pageNo: number = 2;
  public list: Array<any>;
  public totalResult: number;
  public sortName;
  public flag = [];
  constructor(private listService: ListService) {}

  ngOnInit() { 
    // this.sendTotalCount();
  }

// ngDoChanges(){
//   this.sendTotalCount();
// }

  ngOnChanges() {
    //  this._listService.getList().subscribe((data:any) => console.log(data.items));

    // console.log(this.searchUserName);
    // console.log(this.sortBy);

    // total count

    switch (this.sortBy) {
      case "Name(A-Z)":
        this.listService
          .getList(this.searchUserName, this.pageNo)
          .subscribe((data: any) => {
            this.list = data.items;
            this.totalResult = data.total_count;
            // console.log(data);
            this.list = this.list.sort();
            this.sendTotalCount();
          });
        // this.sendTotalCount();
        break;
      case "Name(Z-A)":
        this.listService
          .getList(this.searchUserName, this.pageNo)
          .subscribe((data: any) => {
            this.list = data.items;
            this.totalResult = data.total_count;
            // console.log(data);
            this.list = this.list.sort();
            this.list = this.list.reverse();
            this.sendTotalCount();
          });
        // this.sendTotalCount();
        break;
      case "Rank↑":
        this.listService
          .getList(this.searchUserName, this.pageNo)
          .subscribe((data: any) => {
            this.list = data.items;
            this.totalResult = data.total_count;
            // console.log(this.list[score]);
            for (let i = 0; i < this.list.length; i++) {
              for (let j = i + 1; j < this.list.length; j++) {
                if (this.list[j].score > this.list[i].score) {
                  this.flag = this.list[i];
                  this.list[i] = this.list[j];
                  this.list[j] = this.flag;
                }
              }
            }
            this.sendTotalCount();
          });
        // this.sendTotalCount();
        // console.log(this.list);
        break;
      case "Rank↓":
        this.listService
          .getList(this.searchUserName, this.pageNo)
          .subscribe((data: any) => {
            this.list = data.items;
            this.totalResult = data.total_count;
            // console.log(this.list);
            for (let i = 0; i < this.list.length; i++) {
              for (let j = i + 1; j < this.list.length; j++) {
                if (this.list[j].score < this.list[i].score) {
                  this.flag = this.list[i];
                  this.list[i] = this.list[j];
                  this.list[j] = this.flag;
                }
              }
            }
            this.sendTotalCount();
          });
        // this.sendTotalCount();
        // console.log(this.list);
        break;
    }
   
  }
  sendTotalCount() {
    this.totalCount.emit(this.totalResult);
    console.log(this.totalResult);
  }
}
