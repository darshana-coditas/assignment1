import { Component, OnInit, Input } from "@angular/core";
import { ListService } from "src/app/list.service";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent implements OnInit {
  @Input() userName: string;
  @Input() url: string;
  @Input() imgs: string;
  @Input() scores: number;

  isShow: boolean = true;
  public repos: Object;

  constructor(private listService: ListService) {}

  ngOnInit() {}

  showDetails() {
    console.log("show method");
    this.listService.getRepo(this.userName).subscribe((data2: any) => {
      this.repos = data2;
      this.isShow = !this.isShow;
    });
    console.log(this.isShow);
  }
}
