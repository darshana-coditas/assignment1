import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IList } from "./list";
import { Observable } from "rxjs";
import { IRepos } from "./repo";

@Injectable({
  providedIn: "root"
})
export class ListService {
  private _url: string = "http://api.github.com/search/users?q=darshu";
  private _repurl: string = "https://api.github.com/users/varun/repos";

  constructor(private http: HttpClient) {}

  getList(searchName: string, pageNo: number): Observable<IList[]> {
    return this.http.get<IList[]>(
      "http://api.github.com/search/users?q=" +
        searchName +
        "&per_page=3&page=" +
        pageNo
    );
  }
  getRepo(userName: string): Observable<IRepos[]> {
    return this.http.get<IRepos[]>(
      "https://api.github.com/users/" + userName + "/repos"
    );
  }
}
// https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc
