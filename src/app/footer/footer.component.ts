import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"]
})
export class FooterComponent implements OnInit {
  public isShow: boolean = true;
  pagination_no = [3];
  total: number;
  flag: number;

  @Input() totalSearch: number;

  @Output() page_num = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.pagination_no[0] = 1;
    this.pagination_no[1] = 2;
    this.pagination_no[2] = 3;
  }

  ngDoCheck() {
    console.log("Total search:" + this.totalSearch);
    this.total = Math.ceil(this.totalSearch / 3);
    console.log("Total Pages:" + this.total);
  }

  // ngOnChanes() {
  //   this.isShow = true;
  // }

  prev() {
    this.flag = this.pagination_no[2];
    if (this.flag - 2 > 1) {
      this.pagination_no[0] = this.flag - 5;
      this.pagination_no[1] = this.flag - 4;
      this.pagination_no[2] = this.flag - 3;
    }
  }
  next() {
    this.flag = this.pagination_no[2];
    if (this.flag < this.total - 2) {
      this.pagination_no[0] = this.flag + 1;
      this.pagination_no[1] = this.flag + 2;
      this.pagination_no[2] = this.flag + 3;
    }
  }
  display(no: number) {
    // console.log("test:" + no);
    this.page_num.emit(no);
  }
}
